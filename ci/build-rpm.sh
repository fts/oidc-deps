#!/bin/sh
set -e

# -------------------------------------------------------------------
# Utility script to install build dependencies and build an RPM
# Execute the script from the root of the git repository
# -------------------------------------------------------------------

function print_info {
  printf "======================\n"
  printf "Spec file:\t%s\n" "${SPECFILE}"
  printf "======================\n\n"
}

print_info

# Install build dependencies
if [[ -f /usr/bin/dnf ]]; then
  dnf install -y rpm-build rpmdevtools dnf-utils tree
  dnf builddep "${SPECFILE}" -y
else
  yum install -y rpm-build rpmdevtools yum-utils epel-rpm-macros epel-release tree
  yum-builddep "${SPECFILE}" -y
fi

# Get source listed in the specfile
spectool --sources --patches "${SPECFILE}" -g

# Build SRPM and RPMS from spec file
rpmbuild -ba "${SPECFILE}" --define "_topdir ${PWD}" --define "_sourcedir ${PWD}"
