Name:           python-pyjwkest
Version:        1.4.2
Release:        1%{?dist}.cern
Summary:        Python implementation of JWT, JWE, JWS and JWK

License:        Apache-2.0 AND MIT
Url:            https://github.com/IdentityPython/pyjwkest
Source0:        %{pypi_source pyjwkest}

BuildArch:      noarch

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-six

%description
Python implementation of JWT, JWE, JWS and JWK

%package -n python%{python3_pkgversion}-pyjwkest
Summary:        Python implementation of JWT, JWE, JWS and JWK

%{?python_provide:%python_provide python%{python3_pkgversion}-pyjwkest}
Provides:       pyjwkest = %{version}-%{release}
Obsoletes:      pyjwkest < %{version}-%{release}

%description -n python%{python3_pkgversion}-pyjwkest
Python implementation of JWT, JWE, JWS and JWK

%prep
%autosetup -p1 -n pyjwkest-%{version}

%build
%py3_build

%install
%py3_install

%check
# Try to import "pyjwkest"
PYTHONPATH=%{buildroot}/%{python3_sitelib}/ %{__python3} -c "import jwkest"

%clean
rm -rf $RPM_BUILD_ROOT

%files -n python%{python3_pkgversion}-pyjwkest
%doc README.rst
%{python3_sitelib}/pyjwkest-*
%{python3_sitelib}/jwkest
%{_bindir}/gen_symkey.py
%{_bindir}/jwdecrypt.py
%{_bindir}/jwenc.py
%{_bindir}/peek.py
%{_bindir}/jwk_create.py
%{_bindir}/jwk_export.py
%{_bindir}/jwkutil.py

%changelog
* Fri May 24 2024 Mihai Patrascoiu <mihai.patrascoiu@cern.ch> - 1.4.2-1
- Add ".cern" suffix to the release name

* Thu Aug 17 2023 Joao Lopes <batistal@cern.ch> - 1.4.2-1
- Initial packaging for FTS repositories
