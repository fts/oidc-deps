Name:     python-beaker
Version:  1.11.0
Release:  1%{?dist}.cern
Summary:  WSGI middleware layer to provide sessions

License:  BSD and MIT
Url:      http://beaker.readthedocs.io
Source0:  %{pypi_source Beaker}

BuildArch: noarch

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  %{_bindir}/redis-server
BuildRequires:  %{_bindir}/netstat

%global _description\
Beaker is a caching library that includes Session and Cache objects built on\
Myghty's Container API used in MyghtyUtils. WSGI middleware is also included to\
manage Session objects and signed cookies.

%description %_description

%package -n python%{python3_pkgversion}-beaker
Summary:  WSGI middleware layer to provide sessions

%{?python_provide:%python_provide python%{python3_pkgversion}-beaker}
Provides:           Beaker = %{version}-%{release}
Obsoletes:          Beaker < %{version}-%{release}

%description -n python%{python3_pkgversion}-beaker
%_description

%prep
%autosetup -p1 -n Beaker-%{version}

%build
%py3_build

%install
%py3_install

%check
# Try to import "beaker"
PYTHONPATH=%{buildroot}/%{python3_sitelib}/ %{__python3} -c "import beaker"

%clean
rm -rf $RPM_BUILD_ROOT

%files -n python%{python3_pkgversion}-beaker
%doc README.rst
%{python3_sitelib}/*.egg-info
%{python3_sitelib}/beaker/

%changelog
* Fri May 24 2024 Mihai Patrascoiu <mihai.patrascoiu@cern.ch> - 1.11.0-1
- Add ".cern" suffix to the release name

* Thu Aug 17 2023 Joao Lopes <batistal@cern.ch> - 1.11.0-1
- Initial packaging for FTS repositories
