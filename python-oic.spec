Name:           python-oic
Version:        1.4.0
Release:        1git64c5e3b%{?dist}.cern
Summary:        Python implementation of OAuth2 and OpenID Connect

License:        Apache 2.0
Url:            https://github.com/OpenIDC/pyoidc/
Source0:        https://github.com/CZ-NIC/pyoidc/archive/refs/tags/%{version}.tar.gz

# Try to fix the generic response parsing and handling
Patch0:         https://github.com/CZ-NIC/pyoidc/commit/64c5e3b.patch

BuildArch:      noarch

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools

%description
Python implementation of OAuth2 and OpenID Connect

%package -n python%{python3_pkgversion}-oic
Summary:        Python implementation of OAuth2 and OpenID Connect

%{?python_provide:%python_provide python%{python3_pkgversion}-oic}
Provides:       oic = %{version}-%{release}
Obsoletes:      oic < %{version}-%{release}

%description -n python%{python3_pkgversion}-oic
Python implementation of OAuth2 and OpenID Connect

%prep
%autosetup -p1 -n pyoidc-%{version}

%build
%py3_build

%install
%py3_install

%check
# Try to import "oic"
PYTHONPATH=%{buildroot}/%{python3_sitelib}/ %{__python3} -c "import oic"

%clean
rm -rf $RPM_BUILD_ROOT

%files -n python%{python3_pkgversion}-oic
%license LICENSE.txt
%doc README.rst
%{python3_sitelib}/*.egg-info
%{python3_sitelib}/oic/
%{_bindir}/oic-client-management

%changelog
* Fri May 24 2024 Mihai Patrascoiu <mihai.patrascoiu@cern.ch> - 1.4.0-1git64c5e3b
- Add ".cern" suffix to the release name

* Thu Aug 17 2023 Joao Lopes <batistal@cern.ch> - 1.4.0-1git64c5e3b
- Initial packaging for FTS repositories
