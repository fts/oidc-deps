FTS-REST-Flask OIDC Dependencies
================================

This repository hosts specfiles to build custom RPM dependencies
used by the [FTS-REST-Flask][1] project.

### Python3

The FTS-REST-Flask component requires
the following dependencies which had to be packaged by the FTS team:
```bash
Beaker
oic
pyjwkest
```

These packages were built using the specfiles contained in this repository.
To build the RPM's a Gitlab-ci pipeline can be triggered manually.
The RPM's can be built by running the script [ci/build-rpm-sh](ci/build-rpm.sh).

[1]: https://gitlab.cern.ch/fts/fts-rest-flask
